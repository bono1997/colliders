import ValueGenerator from "./ValueGenerator";
import Position from "../model/Position";

describe('ValueGenerator using seed: "12343k41jkddfjkh"', () => {
  const seed = "12343k41jkddfjkh";

  test("It should generate next collision position: repeatably, correctly", () => {
    const gen1 = new ValueGenerator(seed);
    const gen2 = new ValueGenerator(seed);

    const pos1 = new Position(0, 0);
    const pos2 = new Position(1000, 1000);

    for (let i = 0; i < 100; i++) {
      const res1 = gen1.getNextCollisionPosition(pos1, pos2);
      const res2 = gen2.getNextCollisionPosition(pos1, pos2);
      expect(res1).toEqual(res2);
      expect(res1.x).toBeLessThanOrEqual(1000);
      expect(res1.y).toBeLessThanOrEqual(1000);
      expect(res1.x).toBeGreaterThanOrEqual(0);
      expect(res1.y).toBeGreaterThanOrEqual(0);
    }
  })

  test("It should generate next collision time: repeatably, correctly", () => {
    const gen1 = new ValueGenerator(seed);
    const gen2 = new ValueGenerator(seed);

    for (let i = 0; i < 100; i++) {
      const res1 = gen1.getNextCollisionTime(0, 10);
      const res2 = gen2.getNextCollisionTime(0, 10);
      expect(res1).toEqual(res2);
      expect(res1).toBeLessThanOrEqual(10);
      expect(res1).toBeGreaterThanOrEqual(0);
    }

  })

  test("It should generate next movement visible time: repeatably, correctly", () => {
    const gen1 = new ValueGenerator(seed);
    const gen2 = new ValueGenerator(seed);

    for (let i = 0; i < 100; i++) {
      const res1 = gen1.getNextMovementVisibleTime(0, 10);
      const res2 = gen2.getNextMovementVisibleTime(0, 10);
      expect(res1).toEqual(res2);
      expect(res1).toBeLessThanOrEqual(10);
      expect(res1).toBeGreaterThanOrEqual(0);
    }

  })

  test("It should generate next position on edge: repeatably, correctly, using termination closure", () => {
    const gen1 = new ValueGenerator(seed);
    const gen2 = new ValueGenerator(seed);

    const isFineGenerator = (firstPass: number) => {
      let counter = 0;
      return () => {
        counter++;
        return firstPass <= counter;
      }
    }

    for (let i = 0; i < 100; i++) {
      const res1 = gen1.getNextMovementEdgePoint(isFineGenerator(6), 10, 10);
      const res2 = gen2.getNextMovementEdgePoint(isFineGenerator(5), 10, 10);
      // Sometimes collision is fine, it's pseudorandom after all
      i < 8 && expect(res1).not.toEqual(res2);
      expect(res1.x).toBeLessThanOrEqual(10);
      expect(res1.x).toBeGreaterThanOrEqual(0);
      expect(res1.y).toBeLessThanOrEqual(10);
      expect(res1.y).toBeGreaterThanOrEqual(0);
      expect(res1).toEqual(gen2.getNextMovementEdgePoint(() => true, 10, 10));
    }

  })

})

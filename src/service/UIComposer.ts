import Game from "../model/Game";
import DisplayablePoint from "../model/DisplayablePoint";
import PointState from "../model/PointState";
import Configuration from "../model/Configuration";
import Position from "../model/Position";
import ScoringService from "./ScoringService";
import PointRenderer from "../UI/PointRenderer";
import CanvasManager from "../UI/CanvasManager";

class UIComposer {
  private clickedOnlyOnce: string | undefined;
  private game: Game;
  private displayablePoints: any;
  private readonly configuration: Configuration;
  private readonly scoringService: ScoringService;
  private currentTime: number;
  private readonly canvasManager: CanvasManager;

  public constructor(game: Game,
                     configuration: Configuration,
                     scoringService: ScoringService,
                     currentTime: number,
                     canvasManager: CanvasManager) {
    this.clickedOnlyOnce = undefined;
    this.game = game;
    this.displayablePoints = {};
    this.configuration = configuration;
    this.scoringService = scoringService;
    this.currentTime = currentTime;
    this.canvasManager = canvasManager;
  }

  public setToMoment(moment: number) {
    this.currentTime = moment;
    const movements = this.game.getPoints(moment);
    for (let movement of movements) {
      let displayablePoint = this.displayablePoints[movement.id] as DisplayablePoint;
      if (!displayablePoint) {
        this.displayablePoints[movement.id] = new DisplayablePoint(movement.id, movement.position, PointState.ACTIVE);
        continue;
      } else if (displayablePoint.state !== PointState.OFF && displayablePoint.state !== PointState.MISSED_COLLISION) {
        if (this.isPointOutsideViewport(movement.position)) {
          if (this.clickedOnlyOnce === displayablePoint.id) {
            this.clickedOnlyOnce = undefined;
          }
          displayablePoint.state = PointState.OFF;
        } else if (movement.isOver && displayablePoint.state !== PointState.HIT_COLLISION) {
          this.setMissedCollision(displayablePoint);
          this.scoringService.addMiss();
        }
        displayablePoint.position = movement.position;
      }
    }
  }

  public render() {
    const pointRenderer = new PointRenderer(this.canvasManager, this.configuration);
    pointRenderer.init();
    for (let point of Object.values(this.displayablePoints)) {
      // @ts-ignore
      if (point.state !== PointState.OFF) {
        // @ts-ignore
        pointRenderer.accept(point.position, point.state, () => this.selectPoint(point.id))
      }
    }
    pointRenderer.flush();
  }

  private selectPoint(id: string): void {
    const displayablePoint = this.displayablePoints[id];
    if (!this.clickedOnlyOnce && displayablePoint.state === PointState.ACTIVE) {
      this.clickedOnlyOnce = id;
      displayablePoint.state = PointState.SELECTED;
    } else if (this.clickedOnlyOnce === id) {
      this.clickedOnlyOnce = undefined;
      displayablePoint.state = PointState.ACTIVE;
    } else if (!!this.clickedOnlyOnce) {
      const intersectionMoment = this.game.intersectionMoment(this.clickedOnlyOnce, id);
      if (!!intersectionMoment) {
        this.scoringService.addHit(Math.abs(intersectionMoment - this.currentTime));
        const clicked = this.displayablePoints[this.clickedOnlyOnce];
        this.setHitCollision(clicked);
        this.setHitCollision(displayablePoint);
      } else {
        const clicked = this.displayablePoints[this.clickedOnlyOnce];
        clicked.state = PointState.ACTIVE;
        this.scoringService.addWrongSelection();
      }
      this.clickedOnlyOnce = undefined;
    }
  }

  private setMissedCollision(displayablePoint: DisplayablePoint) {
    displayablePoint.state = PointState.MISSED_COLLISION;
    setTimeout(() => {
      displayablePoint.state = PointState.OFF
    }, this.configuration.disappearAfter);
  }

  private setHitCollision(displayablePoint: DisplayablePoint) {
    displayablePoint.state = PointState.HIT_COLLISION;
    setTimeout(() => {
      this.game.finishMove(displayablePoint.id);
      displayablePoint.state = PointState.OFF;
    }, this.configuration.disappearAfter);
  }

  private isPointOutsideViewport(position: Position): boolean {
    return position.x <= 0
      || position.y <= 0
      || position.y >= this.configuration.gameHeight
      || position.x >= this.configuration.gameWidth
  }
}

export default UIComposer;

import Position from "../model/Position";
import {sha1} from "object-hash";

class ValueGenerator {

  private movementEdgePointSeed: string;
  private movementVisibleTimeSeed: string;
  private collisionTimeSeed: string;
  private collisionPositionSeed: string;

  constructor(seed: string) {
    this.collisionPositionSeed = sha1(seed + "1");
    this.collisionTimeSeed = sha1(seed + "2");
    this.movementVisibleTimeSeed = sha1(seed + "3");
    this.movementEdgePointSeed = sha1(seed + "4");
  }

  public getNextMovementEdgePoint(isFine: (position: Position) => boolean, maxX: number, maxY: number): Position {
    const getPosition = () => {
      const number = ValueGenerator.extractNumber(this.movementEdgePointSeed);
      const side = number % 4;
      switch (side) {
        case 0:
          return new Position(0, number % maxY)
        case 1:
          return new Position(number % maxX, maxY)
        case 2:
          return new Position(maxX, number % maxY)
        default:
          return new Position(number % maxX, 0);
      }
    }
    let position = getPosition();
    this.movementEdgePointSeed = sha1(this.movementEdgePointSeed);
    while (!isFine(position)) {
      position = getPosition();
      this.movementEdgePointSeed = sha1(this.movementEdgePointSeed);
    }
    return position;
  }

  public getNextMovementVisibleTime(min: number, max: number): number {

    if (min >= max) {
      throw new Error("Min greater than max, fatal error!");
    }

    const number = ValueGenerator.extractNumber(this.movementVisibleTimeSeed);
    this.movementVisibleTimeSeed = sha1(this.movementVisibleTimeSeed);

    return number % (max - min) + min;
  }

  public getNextCollisionTime(min: number, max: number): number {

    if (min >= max) {
      throw new Error("Min greater than max, fatal error!");
    }

    const number = ValueGenerator.extractNumber(this.collisionTimeSeed);
    this.collisionTimeSeed = sha1(this.collisionTimeSeed);

    return number % (max - min) + min;
  }

  public getNextCollisionPosition(upperLeft: Position, lowerRight: Position): Position {
    if (upperLeft.x >= lowerRight.x
      || upperLeft.y >= lowerRight.y) {
      throw new Error("Min greater than max, fatal error!");
    }

    const x = ValueGenerator.extractNumber(this.collisionPositionSeed) % (lowerRight.x - upperLeft.x) + upperLeft.x;
    this.collisionPositionSeed = sha1(this.collisionPositionSeed);
    const y = ValueGenerator.extractNumber(this.collisionPositionSeed) % (lowerRight.y - upperLeft.y) + upperLeft.y;
    this.collisionPositionSeed = sha1(this.collisionPositionSeed);

    return new Position(x, y);
  }

  private static extractNumber(source: string): number {
    return parseInt(source.substring(0, 8), 16)
  }
}

export default ValueGenerator;

import Configuration from "../model/Configuration";
import Score from "../model/Score";

const backendURL = "http://localhost:8080/api/collisions"

class ApiService {

  private readonly testID: string;
  private configCounter: number;

  constructor(testID: string) {
    this.testID = testID;
    this.configCounter = 0;
  }

  public getConfig(): Promise<Configuration> {
    this.configCounter++;
    if (this.configCounter > 5) {
      document.body.innerText = "Error fetching test. Please contact researcher."
      return new Promise<Configuration>(res => {});
    } else {
      return fetch(backendURL + "/" + this.testID)
        .then(res => {
          if (res.ok) {
            this.configCounter = 0;
            return <Configuration><unknown>res.json();
          }
          console.error(res);
          return this.getConfig();
        })
        .catch(reason => {
          console.error(reason);
          return this.getConfig();
        });
      /*return new Promise<Configuration>(res => res(new Configuration(10,
        1000,
        800,
        2000,
        10000,
        5000,
        10,
        20,
        10000,
        180000,
        100,
        2000,
        20,
        "abcd"
      )))*/
    }
  }

  public uploadResult(score: Score): Promise<boolean> {
    return fetch(backendURL + "/" + this.testID, {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(score)
    }).then(res => res.ok)
      .catch(reason => {
        console.error(reason);
        return false;
      })
  }

}

export default ApiService;

import Position from "../model/Position";

const randomIntInInterval = (min: number, max: number): number  => {
  return Math.floor(min + Math.random() * (max - min))
}

const floatEquals = (f1: number, f2: number): boolean => {
  return Math.abs(f1 - f2) < 0.001
}

const distance = (p1: Position, p2: Position): number => {
  return Math.sqrt((p1.x - p2.x)**2 + (p1.y - p2.y)**2)
}

export {randomIntInInterval, floatEquals, distance};

import {distance, floatEquals, randomIntInInterval} from "./MathUtils";
import Position from "../model/Position";

test("randomIntInInterval is not under minimum", () => {
  for (let i = 0; i < 100; i++) {
    expect(randomIntInInterval(2, 10)).toBeGreaterThanOrEqual(2);
  }
})

test("randomIntInInterval is not over maximum", () => {
  for (let i = 0; i < 100; i++) {
    expect(randomIntInInterval(2, 10)).toBeLessThan(10);
  }
})

test("floatEquals 1.0000002 and 1.0000001 equals", () => {
  expect(floatEquals(1.0000002, 1.0000001)).toBeTruthy();
})


test("floatEquals 1.02 and 1.001 not equals", () => {
  expect(floatEquals(1.02, 1.001)).toBeFalsy();
})

test("distance (1,1) and (1,1) is 0", () => {
  let pos = new Position(1,1)
  expect(distance(pos, pos)).toEqual(0);
})

test("distance (1,1) and (2,1) is 1", () => {
  let pos1 = new Position(1,1)
  let pos2 = new Position(2,1)
  expect(distance(pos1, pos2)).toEqual(1);
})

test("distance (0,0) and (1,1) is sqrt(2)", () => {
  let pos1 = new Position(0,0)
  let pos2 = new Position(1,1)
  expect(floatEquals(distance(pos1, pos2), Math.sqrt(2))).toBeTruthy();
})

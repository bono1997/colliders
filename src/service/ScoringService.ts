import Score from "../model/Score";
import ApiService from "./ApiService";

class ScoringService {

  // @ts-ignore
  score: Score;
  // @ts-ignore
  private missTracker: number;
  private readonly apiService: ApiService;

  constructor(apiService: ApiService) {
    this.init();
    this.apiService = apiService;
  }

  public init(): void {
    this.score = new Score();
    this.missTracker = 0;
  }

  public addHit(timeBeforeCollision: number): void {
    this.score.hits.push(timeBeforeCollision);
  }

  public addMiss(): void {
    console.log("MISS")
    if (this.missTracker % 2 === 0) {
      this.score.misses++; // Called for each point
    }
    this.missTracker++;
  }

  public addWrongSelection(): void {
    this.score.wrong++;
  }

  public upload(): Promise<boolean> {
    console.log(this.score);
    return this.apiService.uploadResult(this.score);
  }

}

export default ScoringService;

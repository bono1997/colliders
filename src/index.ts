import Configuration from "./model/Configuration";
import Game from "./model/Game";
import ValueGenerator from "./service/ValueGenerator";
import UIComposer from "./service/UIComposer";
import ScoringService from "./service/ScoringService";
import CanvasManager from "./UI/CanvasManager";
import ApiService from "./service/ApiService";

let configuration: Configuration;
let apiService: ApiService;

function bootstrap() {
  const params = new URLSearchParams(window.location.search)
  const id = params.get("id")
  console.log(location.toString() + "  ", id);
  if (!!id && id.length !== 0) {
    history.replaceState(undefined, "Colliders", "");
    apiService = new ApiService(id);
    apiService.getConfig().then(config => {
        configuration = config
        // @ts-ignore
        document.getElementById("startBtn").className = "";
        // @ts-ignore
        document.getElementById("loading").className = "hidden";
      }
    )
  }
}

function start() {

  document.body.innerHTML = "";
  const canvas = document.createElement("canvas");
  canvas.style.border = "2px solid red";
  document.body.appendChild(canvas);
  canvas.requestFullscreen().then(() => {
    configuration.gameWidth = window.screen.width;
    configuration.gameHeight = window.screen.height;
    canvas.width = configuration.gameWidth;
    canvas.height = configuration.gameHeight;

    console.log(configuration)

    const valueGenerator = new ValueGenerator(configuration.seed);

    const game = new Game(configuration, valueGenerator, 0);

    const canvasManager = new CanvasManager(configuration, canvas);

    const scoringService = new ScoringService(apiService);

    const uiComposer = new UIComposer(game, configuration, scoringService, 0, canvasManager);


    let time = 0;
    const intervalID = setInterval(() => {
      uiComposer.setToMoment(time);
      uiComposer.render();
      time += configuration.rerenderRate;
      if (time >= configuration.maxGameElapsed) {
        clearInterval(intervalID);
        scoringService.upload().then(res => {
          if (res) {
            document.body.innerText = "Results have been saved, thank you for participating!"
          } else {
            document.body.innerHTML = "Trouble posting results, please show this screen to researcher<br/>"
              + JSON.stringify(scoringService.score);
          }
        })
      }
    }, configuration.rerenderRate);
  })
    .catch(err => alert(`Error attempting to enable full-screen mode: ${err.message} (${err.name})`))
}

// @ts-ignore
window.start = start;
// @ts-ignore
window.bootstrap = bootstrap;

import PointState from "../model/PointState";
import Position from "../model/Position";
import CanvasManager from "./CanvasManager";
import Configuration from "../model/Configuration";

class PointRenderer {

  private readonly canvasManager: CanvasManager;
  private readonly context: CanvasRenderingContext2D;
  private readonly configuration: Configuration;
  private readonly hitMapX: any;

  constructor(canvasManager: CanvasManager,
              configuration: Configuration) {
    this.context = canvasManager.getBuffer();
    this.canvasManager = canvasManager;
    this.configuration = configuration;
    this.hitMapX = {};
  }

  public init() {
    this.context.fillStyle = "gray";
    this.context.fillRect(0,0, this.configuration.gameWidth, this.configuration.gameHeight);
  }

  public accept(position: Position, state: PointState, onClick: () => void): void {
    switch (state) {
      case PointState.SELECTED:
        this.context.fillStyle = "pink";
        break;
      case PointState.HIT_COLLISION:
        this.context.fillStyle = "green";
        break;
      case PointState.MISSED_COLLISION:
        this.context.fillStyle = "red";
        break;
      case PointState.ACTIVE:
        this.context.fillStyle = "blue"
        break;
      default:
        throw new Error("Leaked point with faulty state to renderer");
    }
    this.context.beginPath()
    this.context.arc(position.x, position.y, this.configuration.pointRadius, 0, 2 * Math.PI);
    this.context.fill();
    this.context.closePath();
    const xStart = Math.floor(position.x - this.configuration.pointRadius * 2);
    const yStart = Math.floor(position.y - this.configuration.pointRadius * 2);
    for (let i = 0; i < this.configuration.pointRadius * 4 + 1; i++) {
      for (let j = 0; j < this.configuration.pointRadius * 4 + 1; j++) {
        if (this.hitMapX[xStart + i]) {
          this.hitMapX[xStart + i][yStart + j] = onClick
        } else {
          this.hitMapX[xStart + i] = {[yStart + j]: onClick}
        }
      }
    }
  }

  public flush(): void {
    const onClick = (position: Position) =>
      this.hitMapX[position.x]?.[position.y]?.();
    this.canvasManager.switch(onClick);
  }

}
export default PointRenderer;

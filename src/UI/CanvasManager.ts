import Configuration from "../model/Configuration";
import Position from "../model/Position";

class CanvasManager {

  private readonly configuration: Configuration;
  private bufferCanvas: HTMLCanvasElement;
  private drawingCanvas: HTMLCanvasElement;

  constructor(configuration: Configuration,
              drawingCanvas: HTMLCanvasElement) {
    this.configuration = configuration;
    this.drawingCanvas = drawingCanvas;
    this.bufferCanvas = this.generateBuffer();
  }

  public getBuffer(): CanvasRenderingContext2D {
    return <CanvasRenderingContext2D> this.bufferCanvas.getContext("2d");
  }

  public switch(clickEventHandler: (position: Position) => void): void {
    // @ts-ignore
    this.drawingCanvas.getContext("2d").drawImage(this.bufferCanvas, 0, 0);
    this.drawingCanvas.onclick = ev => clickEventHandler(new Position(ev.offsetX, ev.offsetY));
    this.bufferCanvas = this.generateBuffer();
  }


  private generateBuffer(): HTMLCanvasElement {
    const canvas = document.createElement("canvas")
    canvas.width = this.configuration.gameWidth;
    canvas.height = this.configuration.gameHeight;
    return canvas;
  }
}

export default CanvasManager;

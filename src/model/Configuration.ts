class Configuration {

  minEdgeDistance: number;

  gameWidth: number;
  gameHeight: number;

  minTimeBetweenCollisions: number;
  maxTimeBetweenCollisions: number;
  minTimeVisibleBeforeCollision: number;
  maxTimeVisibleBeforeCollision: number;

  pointRadius: number;
  minPointDistance: number;

  maxGameElapsed: number
  noOfDummies: number

  disappearAfter: number;
  rerenderRate: number;
  seed: string;

  constructor(minEdgeDistance: number,
              gameWidth: number,
              gameHeight: number,
              minTimeBetweenCollisions: number,
              maxTimeBetweenCollisions: number,
              minTimeVisibleBeforeCollision: number,
              pointRadius: number,
              minPointDistance: number,
              maxTimeVisibleBeforeCollision: number,
              maxGameElapsed: number,
              noOfDummies: number,
              disappearAfter: number,
              rerenderRate: number,
              seed: string) {
    this.minEdgeDistance = minEdgeDistance;
    this.gameHeight = gameHeight;
    this.gameWidth = gameWidth;
    this.minTimeBetweenCollisions = minTimeBetweenCollisions;
    this.maxTimeBetweenCollisions = maxTimeBetweenCollisions;
    this.minTimeVisibleBeforeCollision = minTimeVisibleBeforeCollision;
    this.maxTimeVisibleBeforeCollision = maxTimeVisibleBeforeCollision;
    this.pointRadius = pointRadius;
    this.minPointDistance = minPointDistance;
    this.maxGameElapsed = maxGameElapsed;
    this.noOfDummies = noOfDummies;
    this.disappearAfter = disappearAfter;
    this.rerenderRate = rerenderRate;
    this.seed = seed;
  }

}

export default Configuration;

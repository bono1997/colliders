import Position from "../model/Position";

class Collision {
  position: Position;
  moment: number;
  movements: string[];

  constructor(position: Position, time: number) {
    this.position = position;
    this.moment = time;
    this.movements = [];
  }
}

export default Collision;

enum PointState {
  ACTIVE,
  SELECTED,
  MISSED_COLLISION,
  HIT_COLLISION,
  OFF
}

export default PointState;

import Collision from "./Collision";
import Configuration from "./Configuration";
import ValueGenerator from "../service/ValueGenerator";
import Position from "./Position";
import Movement from "./Movement";
import IdentifiablePosition from "./IdentifiablePosition";

class Game {

  private readonly collisions: Collision[];
  private readonly movements: Movement[];
  private readonly configuration: Configuration;
  private readonly valueGenerator: ValueGenerator;
  private currentTime: number;

  constructor(configuration: Configuration,
              valueGenerator: ValueGenerator,
              currentTime: number) {
    this.collisions = [];
    this.movements = [];
    this.configuration = configuration;
    this.valueGenerator = valueGenerator;
    this.currentTime = currentTime ?? 0;
    this.init();
  }

  /**
   * Returns game state for moment
   *
   * @param moment Timestamp as unix time
   */
  public getPoints(moment: number): IdentifiablePosition[] {
    return this.movements
      // @ts-ignore
      .map(movement => new IdentifiablePosition(movement.id, movement.positionForMoment(moment), movement.isFinished(moment)))
      .filter(position => !!position.position)
  }

  /**
   * Adds collision to game
   *
   * @param currentTime Timestamp as unix time
   */
  private addCollision(currentTime: number): void {
    this.currentTime = currentTime;
    let collision = this.generateCollision();
    this.collisions.push(collision);
    const mov1 = this.generateMovement(collision.position, collision.moment, this.movements);
    const mov2 = this.generateMovement(collision.position, collision.moment, this.movements);
    collision.movements = [mov1.id, mov2.id];
    this.movements.push(mov1, mov2);
  }

  /**
   * Moment of intersection if are intersecting, undefined otherwise
   *
   * @param index1 Index of 1st movement
   * @param index2 Index of 2nd movement
   */
  public intersectionMoment(index1: string, index2: string): number | undefined {
    return this.collisions.find(collision => collision.movements.includes(index1) && collision.movements.includes(index2))?.moment;
  }

  /**
   * Marks move as finished
   * @param id Id of move
   */
  public finishMove(id: string): void {
    const movement = this.movements.find(element => element.id === id)
    if (!!movement) {
      movement.isOver = () => true;
    }
  }

  private init(): void {
    const getLastCollisionTime = () => this.collisions.map(collision => collision.moment)
      .reduce((acc, curr) => acc < curr ? curr : acc, 0)

    while (getLastCollisionTime() < this.configuration.maxGameElapsed + this.configuration.maxTimeVisibleBeforeCollision) {
      this.addCollision(0);
    }

    for (let i = 0; i < this.configuration.noOfDummies; i++) {
      this.addDummyMovement();
    }
  }

  private addDummyMovement(): void {
    const movement = this.generateDummyMovement(this.currentTime + this.configuration.minTimeVisibleBeforeCollision);
    this.movements.push(movement);
  }

  private generateDummyMovement(minFinishTime: number): Movement {

    const finishingTime = this.valueGenerator.getNextCollisionTime(minFinishTime, this.configuration.maxGameElapsed)

    let destinationPos = this.valueGenerator.getNextMovementEdgePoint(
      position => !this.isMovementInCollisionWithOthers(this.generateStationaryMovement(position), this.movements),
      this.configuration.gameWidth,
      this.configuration.gameHeight)

    return this.generateMovement(
      destinationPos,
      finishingTime,
      this.movements
    )
  }

  private generateStationaryMovement = (position: Position) => new Movement(
    new Position(0, 0),
    position,
    0,
    (a) => false,
    position
  )

  private isMovementInCollisionWithOthers(candidate: Movement,
                                          others: Movement[]): boolean {
    for (let movement of others) {
      if (movement.isIntersecting(candidate, this.configuration.minPointDistance, this.configuration)) {
        return true;
      }
    }
    return false;
  }

  private generateMovement(destination: Position,
                           targetMoment: number,
                           otherMovements: Movement[]): Movement {

    const movementDuration = this.valueGenerator.getNextMovementVisibleTime(
      this.configuration.minTimeVisibleBeforeCollision,
      this.configuration.maxTimeVisibleBeforeCollision
    )

    const startMoment = targetMoment - movementDuration;

    const generateCandidate = (startingPosition: Position): Movement => {
      const speed = new Position(
        (destination.x - startingPosition.x) / movementDuration,
        (destination.y - startingPosition.y) / movementDuration
      );

      return new Movement(
        speed,
        startingPosition,
        startMoment,
        (position: Position, elapsedFor: number) => (elapsedFor - movementDuration) > 0.5,
        destination)
    }


    const startingPosition = this.valueGenerator.getNextMovementEdgePoint(
      position => !this.isMovementInCollisionWithOthers(generateCandidate(position), otherMovements),
      this.configuration.gameWidth,
      this.configuration.gameHeight);

    return generateCandidate(startingPosition);
  }

  private generateCollision(): Collision {
    let candidate = new Collision(
      this.generatePermissibleCollisionPosition(),
      this.generatePermissibleCollisionTime()
    )

    while (this.isMovementInCollisionWithOthers(this.generateStationaryMovement(candidate.position), this.movements)) {
      candidate = new Collision(
        this.generatePermissibleCollisionPosition(),
        this.generatePermissibleCollisionTime()
      )
    }

    return candidate;
  }

  private generatePermissibleCollisionTime(): number {
    const collisionTimes = this.collisions
      .map(collision => collision.moment)
      .sort((a, b) => a - b);

    // If no collisions in list
    if (collisionTimes.length === 0) {
      return this.valueGenerator.getNextCollisionTime(
        this.configuration.minTimeVisibleBeforeCollision,
        this.configuration.minTimeVisibleBeforeCollision + this.configuration.maxTimeBetweenCollisions);
    }

    // Try filling in gaps
    for (let i = 1; i < collisionTimes.length; i++) {
      const timeGap = collisionTimes[i] - collisionTimes[i - 1];
      const earliestMoment = this.currentTime + this.configuration.minTimeVisibleBeforeCollision;
      if (timeGap > this.configuration.minTimeBetweenCollisions * 2
        && collisionTimes[i - 1] + this.configuration.minTimeBetweenCollisions > earliestMoment) {
        return this.valueGenerator.getNextCollisionTime(
          collisionTimes[i - 1] + this.configuration.minTimeBetweenCollisions,
          collisionTimes[i] - this.configuration.minTimeBetweenCollisions);
      }
    }

    // Add collision to end
    return this.valueGenerator.getNextCollisionTime(
      collisionTimes[collisionTimes.length - 1] + this.configuration.minTimeBetweenCollisions,
      collisionTimes[collisionTimes.length - 1] + this.configuration.maxTimeBetweenCollisions);
  }

  private generatePermissibleCollisionPosition(): Position {
    const minX = this.configuration.minEdgeDistance;
    const maxX = this.configuration.gameWidth - this.configuration.minEdgeDistance;
    const minY = this.configuration.minEdgeDistance;
    const maxY = this.configuration.gameHeight - this.configuration.minEdgeDistance;

    return this.valueGenerator.getNextCollisionPosition(
      new Position(minX, minY),
      new Position(maxX, maxY)
    )
  }

}

export default Game;

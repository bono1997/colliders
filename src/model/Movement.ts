import Position from "./Position";
import {distance, floatEquals} from "../service/MathUtils";
import Configuration from "./Configuration";

class Movement {

  private static UUID = 0;

  readonly id: string;
  private readonly speed: Position;
  private _isOver: (position: Position, elapsedFor: number) => boolean;
  private readonly startingPosition: Position;
  private readonly startingMoment: number;
  private readonly targetPosition: Position;

  constructor(speed: Position,
              startingPosition: Position,
              startingMoment: number,
              isOver: (position: Position, elapsedFor: number) => boolean,
              targetPosition: Position) {
    this.id = "" + Movement.UUID;
    Movement.UUID++;
    this.speed = speed;
    this.startingPosition = startingPosition;
    this.startingMoment = startingMoment;
    this._isOver = isOver;
    this.targetPosition = targetPosition;
  }

  set isOver(isOver: (position: Position, elapsedFor: number) => boolean) {
    this._isOver = isOver;
  }

  /**
   * Is movement finished
   *
   * @param moment At moment
   */
  public isFinished(moment: number): boolean {
    const position = this.positionForMoment(moment);
    if (position === undefined) {
      return false;
    }
    const elapsedFor = moment - this.startingMoment;
    return this._isOver(position, elapsedFor);
  }

  /**
   * Will this point intersect with other within distance on screen
   *
   * @param other Other movement
   * @param withinDistance Minimum distance
   * @param configuration Screen size provider
   */
  isIntersecting(other: Movement,
                 withinDistance: number,
                 configuration: Configuration): boolean {

    const timeDiff = this.startingMoment - other.startingMoment;

    let a = this.startingPosition.x + (timeDiff < 0 ? this.speed.x * (-1)*timeDiff : 0);
    let b = this.speed.x;
    let c = this.startingPosition.y + (timeDiff < 0 ? this.speed.y * (-1)*timeDiff : 0);
    let d = this.speed.y;
    let e = other.startingPosition.x + (timeDiff > 0 ? other.speed.x * timeDiff : 0);
    let f = other.speed.x;
    let g = other.startingPosition.y + (timeDiff > 0 ? other.speed.y * timeDiff : 0);
    let h = other.speed.y;

    if (distance(new Position(e, g), new Position(a, c)) < withinDistance) {
      return true;
    }

    if (floatEquals(d, h + Math.sqrt(-1 * b ** 2 + 2 * b * f - f ** 2))
      || floatEquals(d, h - Math.sqrt(-1 * b ** 2 + 2 * b * f - f ** 2))) {
      return false;
    }

    // 1st minimum of bla bla bla...it should work is all that's important
    let tMin = ((-1) * h * g + c * h - d * c - b * a - e * f + d * g + e * b + a * f)
      / (d ** 2 + b ** 2 + h ** 2 + f ** 2 - 2 * d * h - 2 * b * f)

    if (tMin < 0) {
      return false;
    }

    let pos1 = this.positionWhenElapsedFor(timeDiff < 0 ? tMin - timeDiff : tMin);
    let pos2 = other.positionWhenElapsedFor(timeDiff > 0 ? tMin + timeDiff : tMin);

    if (timeDiff >= 0 && (this._isOver(pos1, tMin) || other._isOver(pos2, timeDiff + tMin))) {
      return false
    } else if (timeDiff < 0 && (this._isOver(pos1,tMin - timeDiff) || other._isOver(pos2, tMin))) {
      return false
    }

    return pos1.isPositiveWithin(configuration.gameWidth, configuration.gameHeight)
      && pos2.isPositiveWithin(configuration.gameWidth, configuration.gameHeight)
      && (distance(pos1, pos2) < withinDistance)
  }

  private positionWhenElapsedFor(time: number): Position {
    return new Position(
      this.startingPosition.x + this.speed.x * time,
      this.startingPosition.y + this.speed.y * time
    )
  }

  positionForMoment(moment: number): Position | undefined {
    if (moment < this.startingMoment) {
      return undefined;
    }

    const elapsedTime = moment - this.startingMoment;

    const position = this.positionWhenElapsedFor(elapsedTime);

    return this._isOver(position, elapsedTime) ? this.targetPosition : position;
  }

}

export default Movement;

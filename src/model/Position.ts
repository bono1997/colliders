class Position {
  x: number;
  y: number;

  constructor(x: number, y: number) {
    this.x = x;
    this.y = y;
  }

  isPositiveWithin(width: number, height: number): boolean {
    return this.x >= 0
      && this.x < width
      && this.y >= 0
      && this.y < height;
  }
}

export default Position;

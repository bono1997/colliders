import ValueGenerator from "../service/ValueGenerator";
import Configuration from "./Configuration";
import Game from "./Game";
import {floatEquals} from "../service/MathUtils";

const config = new Configuration(
  5,
  1000,
  1000,
  50,
  100,
  5,
  2,
  3,
  10,
  1000,
  30,
  1,
  20,
  "abcd");

describe("Game generation", () => {

  test("It should be repeatable", () => {

    const gen1 = new ValueGenerator(config.seed);
    const gen2 = new ValueGenerator(config.seed);

    const g1 = new Game(config, gen1, 0);
    const g2 = new Game(config, gen2, 0);

    const res11 = g1.getPoints(1).map(element => ({...element, id:""}));
    const res12 = g2.getPoints(1).map(element => ({...element, id:""}));

    const res21 = g1.getPoints(3).map(element => ({...element, id:""}));
    const res22 = g2.getPoints(3).map(element => ({...element, id:""}));

    expect(res11).toEqual(res12);
    expect(res21).toEqual(res22);
  })

  test("Point speed should be constant", () => {
    const gen1 = new ValueGenerator("abcd");

    const g1 = new Game(config, gen1, 0);

    let res: any = {};
    for (let i = 1; i <= 100; i++) {
      const iteration = g1.getPoints(i);
      for (let one of iteration) {
        if (!res[one.id]) {
          res[one.id] = [];
        }
        res[one.id].push(one.position);
      }
    }

    for (let value of Object.values(res)) {
      // @ts-ignore
      for (let i = 1; i < value.length - 1; i++) {
        // @ts-ignore
        const vX1 = value[i - 1].x - value[i].x;
        // @ts-ignore
        const vX2 = value[i].x - value[i + 1].x;
        // @ts-ignore
        const VY1 = value[i - 1].y - value[i].y;
        // @ts-ignore
        const VY2 = value[i].y - value[i + 1].y;

        // @ts-ignore
        const v1 = value[i - 1];
        // @ts-ignore
        const v2 = value[i];
        // @ts-ignore
        const v3 = value[i + 1];

        if (floatEquals(vX1, vX2) && floatEquals(VY1, VY2)) {
          expect(true).toBeTruthy();
        } else {
          expect(v2).toEqual(v3);
        }
      }
    }
  })

  test("Movements should have up to 2 collisions", () => {

    const config1 = new Configuration(10,
      1000,
      600,
      2000,
      10000,
      5000,
      5,
      30,
      15000,
      120000,
      30,
      2000,
      20,
      "abcd"
    );

    const gen1 = new ValueGenerator(config1.seed);
    const g1 = new Game(config1, gen1, 0);

    let totalOneCollision = 0;
    // @ts-ignore
    for (let movement of g1.movements) {
      let i = 0;
      // @ts-ignore
      for (let other of g1.movements) {
        if (movement.isIntersecting(other, config1.minPointDistance, config1)) {
          i++;
          if (other.id !== movement.id) {
            const moment = g1.intersectionMoment(other.id, movement.id);
            expect(moment).toBeTruthy();
          }
        }
      }
      if (i === 1) {
        totalOneCollision++;
      }
      expect(i).toBeLessThanOrEqual(2);
    }
    expect(totalOneCollision).toEqual(config1.noOfDummies);
  })
})

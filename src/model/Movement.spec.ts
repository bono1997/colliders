import Movement from "./Movement";
import Position from "./Position";
import Configuration from "./Configuration";

describe("Movement model for minimum intersecting distance 5", () => {

  let minD = 5;
  let configuration = new Configuration(
    0,
    1000,
    1000,
    1000,
    1000,
    0,
    0,
    3,
    0,
    0,
    0,
    1000,
    20,
    "abcd"
  );

  let m1 = new Movement(
    new Position(1, 1),
    new Position(1, 1),
    0,
    (a, b) => false,
    new Position(0, 0))

  let m2 = new Movement(
    new Position(1, -1),
    new Position(1, 10),
    0,
    (a, b) => false,
    new Position(0, 0))

  let m3 = new Movement(
    new Position(1, 1),
    new Position(50, 1),
    0,
    (a, b) => false,
    new Position(0, 0))

  let m4 = new Movement(
    new Position(1, 0),
    new Position(50, 1),
    0,
    (a, b) => false,
    new Position(0, 0))

  let m5 = new Movement(
    new Position(0.7, 0),
    new Position(1, 5),
    0,
    (a, b) => false,
    new Position(0, 0))

  let m6 = new Movement(
    new Position(0, 0),
    new Position(5, 5),
    0,
    (a, b) => false,
    new Position(0, 0))

  let m7 = new Movement(
    new Position(-1, 0),
    new Position(100, 5),
    0,
    (a, elapsedFor) => elapsedFor > 5,
    new Position(0, 0)
  )

  let m8 = new Movement(
    new Position(1, 0),
    new Position(50, 5),
    0,
    (a, elapsedFor) => false,
    new Position(0, 0)
  )

  let m9 = new Movement(
    new Position(-1, 0),
    new Position(100, 5),
    0,
    (a, elapsedFor) => false,
    new Position(0, 0)
  )

  let m10 = new Movement(
    new Position(1, 0),
    new Position(0, 100),
    1,
    (a, b) => false,
    new Position(1000, 100)
  )

  let m11 = new Movement(
    new Position(0, 1),
    new Position(100, 0),
    0,
    (a, b) => false,
    new Position(100, 1000)
  )

  let m12 = new Movement(
    new Position(0, 1),
    new Position(100, 0),
    2,
    (a, b) => false,
    new Position(100, 1000)
  )

  test("m1 and m2 are intersecting", () => {
    expect(m1.isIntersecting(m2, minD, configuration)).toBeTruthy();
  })

  test("m2 and m1 are intersecting", () => {
    expect(m2.isIntersecting(m1, minD, configuration)).toBeTruthy();
  })

  test("m1 and m5 are intersecting", () => {
    expect(m1.isIntersecting(m5, minD, configuration)).toBeTruthy();
  })

  test("m5 and m1 are intersecting", () => {
    expect(m5.isIntersecting(m1, minD, configuration)).toBeTruthy();
  })

  test("m5 and m2 are intersecting", () => {
    expect(m5.isIntersecting(m2, minD, configuration)).toBeTruthy();
  })

  test("m2 and m5 are intersecting", () => {
    expect(m2.isIntersecting(m5, minD, configuration)).toBeTruthy();
  })

  test("m1 and m4 aren't intersecting", () => {
    expect(m1.isIntersecting(m4, minD, configuration)).toBeFalsy();
  })

  test("m4 and m1 aren't intersecting", () => {
    expect(m4.isIntersecting(m1, minD, configuration)).toBeFalsy();
  })

  test("m3 and m4 are intersecting", () => {
    expect(m3.isIntersecting(m4, minD, configuration)).toBeTruthy();
  })

  test("m3 and m5 aren't intersecting", () => {
    expect(m3.isIntersecting(m5, minD, configuration)).toBeFalsy();
  })

  test("m4 and m5 aren't intersecting", () => {
    expect(m4.isIntersecting(m5, minD, configuration)).toBeFalsy();
  })

  test("m6 and m1 are intersecting", () => {
    expect(m6.isIntersecting(m1, minD, configuration)).toBeTruthy();
  })

  test("m6 and m1 are intersecting", () => {
    expect(m6.isIntersecting(m1, minD, configuration)).toBeTruthy();
  })

  test("m7 and m8 aren't intersecting", () => {
    expect(m7.isIntersecting(m8, minD, configuration)).toBeFalsy();
  })

  test("m9 and m8 are intersecting", () => {
    expect(m9.isIntersecting(m8, minD, configuration)).toBeTruthy();
  })

  test("m10 and m11 are intersecting", () => {
    expect(m10.isIntersecting(m11, minD, configuration)).toBeTruthy();
  })
  test("m10 and m12 are intersecting", () => {
    expect(m10.isIntersecting(m12, minD, configuration)).toBeTruthy();
  })
  test("m11 and m12 are intersecting", () => {
    expect(m11.isIntersecting(m12, minD, configuration)).toBeTruthy();
  })
})

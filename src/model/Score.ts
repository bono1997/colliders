class Score {
  readonly hits: number[];
  misses: number;
  wrong: number;

  constructor() {
    this.hits = [];
    this.misses = 0;
    this.wrong = 0;
  }

}

export default Score;

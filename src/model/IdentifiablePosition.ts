import Position from "./Position";

class IdentifiablePosition {
  readonly id: string;
  readonly position: Position;
  readonly isOver: boolean;

  constructor(id: string, position: Position, isOver: boolean) {
    this.id = id;
    this.position = position;
    this.isOver = isOver;
  }
}

export default IdentifiablePosition;

import PointState from "./PointState";
import Position from "./Position";

class DisplayablePoint {
  readonly id: string;
  position: Position;
  state: PointState;

  constructor(id: string,
              position: Position,
              state: PointState) {
    this.id = id;
    this.position = position;
    this.state = state;
  }

}

export default DisplayablePoint;

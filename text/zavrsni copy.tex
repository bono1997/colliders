\documentclass[times, utf8, zavrsni, numeric]{fer}
\usepackage{booktabs}
\usepackage{algorithmic}
\usepackage{listings}
\usepackage{algorithm}
\lstdefinelanguage{Javascript}{
  keywords={break, case, catch, continue, debugger, default, delete, do, else, finally, for, function, if, in, instanceof, new, return, switch, this, throw, try, typeof, var, void, while, with, const, let},
  morecomment=[l]{//},
  morecomment=[s]{/*}{*/},
  morestring=[b]',
  morestring=[b]",
  sensitive=true
}

\begin{document}

\thesisnumber{7056}

\title{PROŠIRIVA MREŽNA PLATFORMA ZA MJERENJE SITUACIJSKE SVJESNOSTI}

\author{Bono Vidaković}

\maketitle

% \izvornik

\zahvala{Zahvaljujem roditeljima na podršci tijekom studiranja i mentoru prof. Siniši Popoviću na mentoriranju pri izradi rada.}

\tableofcontents

\chapter{Uvod}
Situacijska svjesnost je percepcija okolišnih elemenata i događaja u odnosu na vrijeme ili prostor, shvaćanje njihovog značenja i projekcija njihovog budućeg statusa. \citep{endsley95}
Nedostatna situacijska svjesnost je identificirana kao jedan od primarnih faktora u nesrećama uzrokovanim ljudskom pogreškom. \citep{hasmpr91}
Pokazalo se da je visoka razina situacijske svjesnosti temelj za uspješno bavljenje zanimanjima poput:
\begin{itemize}
\item Pomorstva
\item Zdravstva
\item Naftnom ekstrakcijom na otvorenom moru \engl{offshore drilling} \citep{floc01}
\item Hitne pomoći
\item Vojnog upravljanja \citep{blwo04}
\item Avijacije
\item Kontrole leta
\end{itemize}

U ovom je napravljena mrežna platforma za potporu provedbi mjerenja situacijske svjesnosti.
Na platformi su implementirana 2 testa radi demonstracije proširivosti.
Prvi je standardni test radne memorije s nizom slova i prostornih konfiguracija kvadratića koji se ponavljaju u nasumičnim trenucima s razmakom N \engl{Dual-N-Back test},
dok je drugi mjerenje sposobnosti predviđanja i prioritiziranja konflikata točkastih reprezentacija zrakoplova u okviru pojednostavljene dvodimenzionalne simulacije zračnog prometa.
Fokus rada je 2.\ test kojim je ujedno i demonstrirana laka proširivost platforme.

\chapter{Pojednostavljena simulacija zračnog prometa}

U svrhu lakše kvantifikacije situacijske svjesnosti na platformi je implementirana simulacija nadzora zračnog prometa.
Simulacija se sastoji od putujućih točki različitih boja čije je značenje objašnjeno u tablici \ref{tbl:boje_tocki}

\begin{table}[htb]
\caption{Boje točki}
\label{tbl:boje_tocki}
\centering
\begin{tabular}{llr} \hline
Boja & Opis \\ \hline
plava & Neoznačena, aktivna točka s nepoznatim ishodom \\
crvena & Promašena kolizija \\
ružičasta & Trenutno označena točka \\
zelena & Ispravno sparena točka \\ \hline
\end{tabular}
\end{table}

Svaka točka se giba jednolikom brzinom s početkom u nekoj nasumičnoj točki na rubu ekrana sukladno parametrima predanim aplikaciji pri učitavanju.
Promjenjivi parametri koji utječu na simulaciju su:
\begin{enumerate}
\label{enum:parametri}
\item Minimalna udaljenost kolizije od ruba polja
\item Visina polja
\item Širina polja
\item Minimalno vrijeme između kolizija
\item Maksimalno vrijeme između kolizija
\item Minimalno vrijeme prikaza točke na ekranu (može se tumačiti i kao minimalno vrijeme za reakciju)
\item Maksimalno vrijeme prikaza točke na ekranu (može se tumačiti i kao maksimalno vrijeme za reakciju)
\item Radijus kruga koji označava točku
\item Minimalna udaljenost točaka koje se ne križaju
\item Trajanje testa
\item Broj distraktorskih točki (točke koje neće biti u koliziji ni s jednom drugom točkom)
\item Vrijeme nakon kojega pogođene / promašene kolizije nestaju s ekrana
\item Sjeme \engl{seed} za generator testa
\item Temeljni period osvježavanja (broj milisekundi nakon kojega se generira nova slika)
\end{enumerate} 

Da bi platforma bila upotrebljiva za prikupljanje svrsishodnih podataka o situacijskoj svjesnosti nužno je da ima sljedeće karakteristike:
\begin{description}
\item[Ponovljivost:] Točke se moraju gibati na isti način u istim trenucima pri svakom pokretanju s istom konfiguracijom i sjemenom
\item[Brzina izvođenja:] Ispitanik ne smije primijetiti treperenje niti zastoje na prosječnom potrošačkom računalu
\item[Konfigurabilnost:] Istraživač mora moći mijenjati parametre simulacije
\item[Preciznost:] Simulacija mora prikazivati samo dozvoljene ishode
\item[Višekorisnički rad:] Na platformi mora biti omogućen rad više istraživača i ispitanika paralelno.
\end{description}
 U nastavku je opisan način na koji su realizirana navedena svojstva na svojoj platformi.
 
\section{Ponovljivost}

Ponovljivost je definirana kao \glqq usko slaganje između rezultata uzastopnih mjerenja iste mjerene veličine izvedenih u istim mjernim uvjetima \grqq \citep{rijcro1}.\\
Na platformi je realizirana ponovljivost primjenom jednosmjernih hash funkcija na sjeme simulacije u generatoru pseudonasumičnih brojeva.\\
Hash funkcija je funkcija koja preslikava domenu u niz bitova tako da je iz rezultantnog niza nemoguće dobiti originalni ulazni parametar na ikoji način osim ponovnim hashiranjem ulaznog parametra.\\
Idealna hash funkcija ima sljedeća svojstva:
\begin{description}
\item[Determinizam] Isti ulazni parametar će uvijek rezultirati istim izlaznim parametrom
\item[Brzina] Brzo računa hash vrijednost ulaznog parametra
\item[Jednosmjernost] Nemoguće je generirati ulazni parametar koji daje ciljanu hash vrijednost
\item[Injektivnost] Funkcija je injekcija
\item[Uniformnost] Mala promjena na ulaznom parametru uzrokuje promjenu toliko veliku u rezultantnoj hash vrijednosti da nova hash vrijednost djeluje nekorelirano sa starom hash vrijednošću \engl{avalanche effect} \citep{asdjhb11}
\end{description}
Za hashing funkciju je odabran algoritam SHA-1 (Secure Hash Algorithm 1) jer ima sljedeća svojstva:
\begin{description}
\item[Uniformna raspršenost:] Funkcija je injekcija na velikom dijelu domene \citep{wxyyly05}
\item[Brzina]: Funkcija je jedna od najbržih hashing funkcija u širokoj upotrebi
\end{description}

Generator pseudoslučajnih, no ponovljivih vrijednosti koristi algoritam:

\begin{algorithm}
\caption{Pseudorandom generator}
\label{alg:pseudorandom_alg}
\begin{algorithmic}
\STATE{seed := sha1(seed)}
\RETURN{seed}
\end{algorithmic}
\end{algorithm}

\section{Brzina izvođenja}

Performantnost u kontekstu ovog rada se definira kao mogućnost prikazivanja stanja klijentske aplikacija i reagiranja na naredbe (poput klikanja pokazivača) unutar temeljnog perioda spomenutog na početku poglavlja na prosječnom osobnom računalu potrošačkog razreda, gdje je temeljni period barem 25 milisekundi.
Temeljni period osvježavanja 25 milisekundi je isto što i 40 FPS \footnote{ \engl{Frames Per Second} Broj rendera u sekundi }.
Stvar koja posebno ograničava opcije za postupke postizanja željene performanse jest činjenica da je teško ostvariti višedretveno izvođenje aplikacije u okolini za koju je predviđena (internet preglednik) zbog specifičnosti JavaScripta.

Kako bi postigao željene performansne karakteristike korištene su:
\begin{itemize}
\item Prostorna hash mapa
\item Dvostruki spremnik \engl{double buffering}
\item Priručna spremišta \engl{caching}
\end{itemize}

\subsection{Prostorna hash mapa}
\label{subsec:spatial_hash_map}

Prostorna hash mapa je struktura podataka koja omogućuje pristup informaciji o točki u prostoru s konstantnom algoritamskom složenošću $O(C)$.
Ta optimizacija je nužna kako bi klijentska aplikacija u svakom trenutku mogla brzo isprocesuirati događaje inicirane od strane ispitanika (poput klika pokazivačem na točku).

Prostorna hash mapa je implementirana kao rijetko 2D polje \engl{array} \emph{$H_{xy}$} čiji su indexi \emph{x, y} vrijednosti koordinata \emph{T(x, y)} točaka zaslona, a pohranjena vrijednost \emph{H[x][y]} je procedura zadužena za procesuiranje korisničke interakcije.
Prilikom svakog ponovnog prikaza \engl{render} prostorna hash mapa se generira iz početka.

\subsection{Dvostruki spremnik}

Metoda dvostrukih spremnika jest tehnika u računalnoj znanosti gdje koristimo 2 spremnika za prikaz podataka koje osvježavamo i prikazujemo naizmjenično.\\
Ona nam pomaže osigurati se da ćemo uvijek prikazati u potpunosti nacrtanu sliku što za rezultat ima značajno manje digitalnih ostataka \engl{artifact} te je prikaz ugodniji oku.\\

\subsection{Priručna spremišta}

Korištenje priručnih spremišta \engl{caching} je tehnika kojom se postiže ubrzanje nekih računsko intenzivnih operacija.
Prilikom poziva \textbf{funkcije} odnosno prilikom vraćanja vrijednosti iz \textbf{funkcije} spremamo parametre te vraćenu vrijednost u priručni spremnik.
Kasnije tijekom izvođenja programa ako je funkcija pozvana s istim parametrima ne izvršavamo je, nego vraćamo prethodno pohranjenu vrijednost za te parametre.

\section{Konfigurabilnost}

Kako bi platforma bila primjenjiva duži period u svrhu provođenja istraživanja nužno je da bude konfigurabilna. \\
Pri oblikovanju programske podrške vođeno je računa o tome da nijedan parametar naveden u listi \ref{enum:parametri} nije pohranjen trajno u kodu platforme,
nego je promjenjiv tijekom rada.

\section{Preciznost}

Preciznost u kontekstu ovog rada znači da će aplikacija strogo poštivati zadane konfiguracijske parametre te će ponašanje prikazanih točaka biti definirano sljedećim ograničenjima:
\begin{itemize}
\item Točka cijelo vrijeme dok je prikazana može imati maksimalno 1 koliziju s drugom putujućom točkom
\item Točka se kreće jednolikom brzinom
\item Ako točka ima koliziju s drugom točkom njihovo gibanje se zaustavlja u mjestu kolizije
\item Ako točka nema koliziju s drugom točkom giba se od jednog ruba prikaza do drugog (ne nužno nasuprotnog) ruba
\end{itemize}

Da bi se ostvarila preciznost za otkrivanje kolizija korišten je diferencijalni račun. \\ \\
\emph{
Neka se točka $T_1(x_1, y_1)$ giba brzinom $\vec{v_1}=v_{x1}\vec{i} + v_{y1}\vec{j}$ od trenutka $t_1$ s pozicije $T(x_10, y_10)$.\\
Neka se točka $T_2(x_2, y_2)$ giba brzinom $\vec{v_2}=v_{x2}\vec{i} + v_{y2}\vec{j}$ od trenutka $t_2$ s pozicije $T(x_20, y_20)$.\\
Tada udaljenost D u trenutku t možemo izraziti kao:\\ \\
$D(t)=\sqrt{(x_1(t) - x_2(t + \Delta t))^2 + (y_1(t) - y_2(t + \Delta t))^2} \\
\Delta t = t_2 - t_1 $ \\ \\
Kako tražimo minimalnu udaljenost potrebno je odrediti trenutak $t_{min}$ od početka gibanja tijela koje se počelo zadnje gibati.\\
S obzirom na to da se radi o jednolikim pravocrtnim gibanjima znamo da funkcija udaljenosti točaka u vremenskoj domeni ima 1 ekstrem koji označava minimalnu udaljenost dviju točki:\\ \\
$\frac{dD}{dt} = 0 \\ \implies t_{min} = \frac{-v_{y2}y_{02} + v_{y2}(y_{01} + v_{y1}\Delta t)  - v_{y1}(y_{01} + v_{y1}\Delta t) - v_{x1}(x_{01} + v_{x1}\Delta t) - v_{x2}x_10 + v_{y1}x_{20} + x_{20} * v_{x1} + v_{x2}(x_{01} + v_{x1}\Delta t)}
{v_{y1} ^ 2 + v_{x1} ^ 2 + v_{y1} ^ 2 + v_{x2} ^ 2 - 2 v_{y1} v_{y2} - 2 v_{x1} v_{x2}}  $ \\
Uz uvjete:\\ $v_{x1} \neq v_{y2} + \sqrt{- v_{x1} ^ 2 + 2 * v_{x1} * v_{x2} - v_{x2} ^ 2} \\ v_{x1} \neq v_{y2} - \sqrt{- v_{x1} ^ 2 + 2 * v_{x1} * v_{x2} - v_{x2} ^ 2}$
}

\section{Višekorisnički rad}

Platforma mora podržavati višekorisnički rad.\\
Kriteriji ovog zahtjeva su:
\begin{itemize}
\item Preko 100 korisnika može istovremeno koristiti sustav
\item Korisnici ne smiju svojim radom utjecati na druge korisnike sustava
\item Istraživači imaju pristup isključivo svojim podacima
\item Ispitanici imaju pristup isključivo svojoj instanci testa za isključivo jednu upotrebu
\end{itemize}

U radu je zahtjev ispunjen koristeći jednokratne linkove pomoću kojih se pokreće jedno ispitivanje.
Istraživačima je omogućen višekorisnički rad pomoću korisničkih računa.
Svaki istraživač se ulogira pomoću emaila i lozinke.
Autentikacijski podaci se šalju svakim mrežnim zahtjevom klijentske aplikacije u zaglavlju HTTP zahtjeva koristeći HTTP Basic autentikacijsku shemu.
HTTP Basic je autentikacijski standard široko korišten na webu.
Radi tako da se sa svakim mrežnim zahtjevom za koji je potrebno da je korisnik autentificiran šalje zaglavlje oblika:
\begin{lstlisting}
Authorization: Basic <username>:<password>
\end{lstlisting}

Kako bi se osigurali da u slučaju sigurnosnog incidenta ostali računi korisnika ostanu sigurni lozinke ne pohranjujemo u nešifriranom \engl{plaintext} obliku.
Umjesto njega koristimo \emph{bcrypt} \citep{prnima99} algoritam za hashiranje lozinki.
Odabran je bcrypt jer standardno generira hash lozinke koristeći \emph{salt}\footnote{salt u doslovnom prijevodu znači sol, no tipično se riječ koristi neprevedena} te jer ima promjenjiv broj iteracija, odnosno promjenjivu težinu generiranja hasha.
\emph{salt} je podatak koji dodajemo čitkom tekstu \engl{plaintext} prije nego ga hashiramo. 

Salt se dodaje kako bi se spriječili napadi koji iskorištavaju rođendanski paradoks \engl{birthday paradox}, odnosno kako bi se napadaču onemogućila upotrebu duginih tablica \engl{rainbow table}.
Promjenjiv broj iteracija je važan jer nam omogućuje da pojačavamo težinu generiranja hasha s vremenom bez promjene algoritma.
Kada koristimo hash tablice naša platforma nikada ne sprema lozinku već samo sprema njen hash.
Po primitku lozinke od klijentske aplikacije generira njen hash te provjerava podudara li se s već spremljenim hashem za taj korisnički račun.

\chapter{Arhitektura sustava}

Platforma se sastoji od 5 računalnih programa:
\begin{enumerate}
\item Baza podataka
\item Serverska aplikacija \engl{backend}
\item Upravljačko sučelje
\item Test situacijske svjesnosti
\item Dual-N-Back test
\end{enumerate}

\begin{figure}[htb]
\centering
\includegraphics[width=12cm]{img/arhitektura2.png}
\caption{Arhitektura platforme}
\label{fig:dijagram_arhitekture}
\end{figure}

Programi su međusobno povezani koristeći internet, izuzev veze serverske aplikacije i baze podataka koja je ostvarena koristeći Unix socket.
Sučelja između klijentskih aplikacija (upravljačkog sučelja, testa situacijske svjesnosti i Dual-N-Back testa) i serverske aplikacije su oblikovana prema REST standardu.

REST standard jest standard za oblikovanje HTTP sučelja definiran od strane Roy Fieldinga u doktorskoj disertaciji "Architectural Styles and the Design of Network-based Software Architectures" 2000. godine \citep{firt00}.

\section{Baza podataka}

Korištena je relacijska baza podataka Postgres.
Odabrana je relacijska baza jer je većina podataka strukturirana i poznate veličine.
Postgres je odabran jer je popularan, besplatan i dobro dokumentiran.

\begin{figure}[htb]
\centering
\includegraphics[width=14cm]{img/database.png}
\caption{Shema baze podataka}
\label{fig:dijagram_baze}
\end{figure}

Pohrana podataka promjenjive strukture je na platformi realizirana koristeći 2 tipa polja:
\begin{description}
\item[BYTEA] Podaci pohranjeni u binarnom obliku koristeći standardni Java podršku za serijalizaciju
\item[INTEGER ARRAY] Podaci pohranjeni kao standardni array tip
\end{description}

\section{Serverska aplikacija}

Serverska aplikacija je dio platforme koji se pokreće na serveru i sadrži autentikaciju, autorizaciju te brine o spremanju podataka u bazu.

Arhitektura aplikacije je MVC, odnosno Model-View-Controller. \\
Aplikacija je slojevita te se sastoji od sljedećih slojeva poredanih počevši od baze:
\begin{enumerate}
\item Repozitorij
\item Servisni
\item Kontrolerski
\end{enumerate}

Korišten je programski okvir \engl{framework} Spring Boot te programske knjižnice \engl{library}:
\begin{description}
\item[Jackson] Koristi se za pakiranje \engl{marshalling}, odnosno raspakiravanje \engl{unmarshalling} podataka između Java POJO i JSON standardnih objekata
\item[Hibernate] Objektno relacijski omotač, koristi se za komunikaciju s bazom
\item[Lombok] Annotation Preprocessor, generator koda korišten da smanji količinu koda koji se mora pisati, a standardnog je oblika za većinu projekata (konstruktori, getteri, setteri, graditelj obrazac i slično)
\end{description}
Spring Boot nam znatno olakšava implementaciju jednostavnijih serverskih aplikacija zbog pristupa \emph{"Convention over configuration"}, prevedeno na hrvatski "Konvencija umjesto konfiguracije".
Konvencija umjesto konfiguracije je filozofija oblikovanja programskog okvira u kojoj se konfiguracija okvira postavlja u skladu s industrijskim standardima, dok se programeru ostavlja mogućnost prilagođavanja rada ako mu je potrebno.
Zahvaljujući tom pristupu imamo s vrlo malo koda pristup velikom broju značajki kvalitetne web aplikacije.
 
\subsection{Model podataka}

Model podataka jest popis svih struktura podataka i njihovih međusobnih ovisnosti.\\
U ovoj aplikaciji se model podataka može podijeliti na 2 osnovne skupine:
\begin{description}
\item[DTO: ] \engl{Data Transfer Object} jest naziv za strukture podataka koje koristimo za prijenos informacija između više sustava
\item[Entity: ] Entitetski objekti su objekti čija jer struktura ista kao i struktura pojedinih relacija u bazi podataka.
\end{description}

DTO koristimo s Jackson serijalizacijskom knjižnicom \engl{library} kako bi se Java objekti pretvorili u JSON standardne zapise.
Korištenje JSON standarda s REST arhitekturom HTTP sučelja je ključno za osiguravanje proširivosti aplikacije.
Naime JSON standardni zapisi omogućuju da je internetsko sučelje serverske aplikacije vrlo lako koristiti.

\begin{figure}[H]
\centering
\includegraphics[width=14cm]{img/dto_class_uml.png}
\caption{UML dijagram DTO objekata}
\label{fig:dto_uml}
\end{figure}

Entitetski objekti se koriste s Hibernate objektno-relacijskim maperom \engl{ORM - Object Relational Mapper}.\\
Hibernate je dobar izbor kod manjih projekata poput ovoga jer znatno smanjuje količinu koda potrebnu za preslikavanje baze podataka u Java objekte te nam nudi skup gotovih SQL upita za jednostavne operacije s bazom.\\

\begin{figure}[H]
\centering
\includegraphics[width=14cm]{img/entity_class_uml.png}
\caption{UML dijagram entitetskih objekata}
\label{fig:entity_uml}
\end{figure}

\subsection{Servisni sloj}

Servisni sloj zadrži logiku aplikacije.
U njemu su sadržana poslovna pravila aplikacije poput hashiranja lozinke, preslikavanja podataka, asocijacija objekata i slično.

\begin{figure}[htb]
\centering
\includegraphics[width=14cm]{img/service_class_uml1.png}
\caption{UML dijagram servisnih objekata}
\label{fig:service_uml}
\end{figure}

\subsection{Kontrolerski sloj}

Kontrolerski sloj je zadužen za mapiranje podataka u oblik prikladan za slanje internetom te mapiranje ruta.
U ovom radu kontrolerski sloj radi "pakiranje" \engl{marshalling}, odnosno "raspakiranje" \engl{unmarshalling} podataka između Java POJO \engl{Plain Old Java Object} i JSON standardnih objekata.
Mapiranje ruta jest proces gdje se iz HTTP paketa iščitava na koju je rutu poslan te se sadržaj prosljeđuje odgovarajućim dijelovima logike (u MVC arhitekturi servisnom sloju, odnosno modelu).

\begin{figure}[H]
\centering
\includegraphics[width=14cm]{img/controller_class_uml.png}
\caption{UML dijagram kontrolerskih objekata}
\label{fig:controller_uml}
\end{figure}

\section{Test situacijske svjesnosti}

Test situacijske svjesnosti je klijentska aplikacija koja se pokreće u ispitanikovom internet pregledniku.\\
Arhitektura je MVC. \\
Vanjske programske knjižnice korištene pri izradi ovog programa su:
\begin{description}
\item[Jest] Radni okvir koji omogućuje laku implementaciju testova
\item[Typescript] Omogućuje nam korištenje strogo definiranih tipova u JavaScriptu
\item[Webpack] JavaScript kod prevodi u oblik u kojemu ga većina browsera može interpretirati
\item[object-hash] JavaScript programska knjižnica koja pruža podršku za generiranje hasheva standardnim hashing algoritmima (u radu je korišten SHA-1)
\end{description}
Tijekom izrade rada bilo je ključno ispravno postaviti arhitekturu ove aplikacije te definirati sučelja između njenih komponenti zbog kompleksnosti logike te performansnih potreba.

\begin{figure}[H]
\centering
\includegraphics[width=14cm]{img/colliders_dependency.png}
\caption{Dijagram ovisnosti modula Testa situacijske svjesnosti}
\label{fig:colliders_deps}
\end{figure}

Postupak pokretanja klijentskog programa:
\begin{enumerate}
\item Učitava se sav JavaScript kod, html i CSS
\item Klijentska aplikacija iz URL-a \engl{Uniform Resource Locator} izvlači parametar \emph{id}
\item Klijentska aplikacija zahtjeva od servera konfiguraciju za test koji odgovara parametru \emph{id} izvučenom iz URL-a
\item Po primitku konfiguracije od servera prikazuje se gumb "START"
\item Klikom na gumb start aplikacija u izračunava sve pokrete za čitavo trajanje testa te pokreće test
\item Klijentska aplikacija reagira u skladu s korisnikovim radnjama
\item Klijentska aplikacija šalje rezultate serverskoj aplikaciji te završava
\end{enumerate}
Ovakav niz koraka je bio nužan kako bi osigurali minimalnu povezanost serverske i klijentskih aplikacija, odnosno maksimalnu konfigurabilnost testa.

Postupak generiranja testa:
\begin{enumerate}
\item Koristeći pseudonasumični generator vrijednosti odredi se niz trenutaka (u danjem tekstu vremenska crta kolizija) od početka pokretanja aplikacije $t_0=0$ do trenutka $t_1=t_{trajanje} + t_{max_vidljiv_element}$ koji zadovoljavaju uvjete u konfiguraciji
\item Za svaki od trenutaka se odredi prostorna koordinata (x,y) kolizije takva da zadovoljava konfiguracijske uvijete
\item Za svaku od prostornih koordinata kolizije se određuje par gibanja koja počinju u različitim rubnim točkama vidnog polja te u različitim trenucima, a nisu u koliziji ni s jednim postojećim gibanjem
\item Dodaje se broj gibanja koja nisu u koliziji ni s jednim drugim gibanjem već dodanim u test. \\Broj je definiran u konfiguraciji.
\end{enumerate}
Ovakav način generiranja testa se pokazao kao najbrži i najučinkovitiji.

\section{Upravljačko sučelje}
Upravljačko sučelje je klijentska aplikacija koja se pokreće u ispitivačevom internet pregledniku.

Arhitektura aplikacije je Flux.
Flux arhitektura je popularni, moderni arhitekturni obrazac često primjenjivan u aplikacijama izrađenim korištenjem knjižnice React.
Odlikuje ga jednosmjerni tok podataka, jasno definirano stablo komponenti te vrlo često centralno spremište za stanje čitave aplikacije.

Korištene programske knjižnice:
\begin{description}
\item[React] Programska knjižnica za izradu web grafičkih sučelja
\item[react-router] Programska knjižnica za lakšu implementaciju ruta u internet pregledniku kod jednostraničnih aplikacija \engl{SPA - Single Page Application}
\item[tachyons] Kolekcija CSS klasa oblikovanih tako da olakšaju pisanje CSS koda koristeći \emph{utility} CSS paradigmu
\end{description}

\begin{figure}[H]
\centering
\includegraphics[width=14cm]{img/admin_dependency.png}
\caption{Dijagram ovisnosti modula Upravljačkog sučelja}
\label{fig:admin_deps}
\end{figure}

Za ovu komponentu sustava je bilo najbitnije osigurati laku proširivost.
Flux arhitektura je izuzetno pogodna za to jer omogućava vrlo laku primjenu obrasca komponenta višeg reda \engl{HOC - Higher Order Component}.

Komponente višeg reda su obrazac popularan u funkcijskoj programskoj paradigmi, a omogućava nam da proširimo ponašanje neke funkcije.
U ovom radu HOC obrazac omogućuje lako odjeljivanje stanja jedne komponente od njegove reprezentacije.
U ovoj klijentskoj aplikaciji napravljena je komponenta Game koja je komponenta višeg reda koja sadrži logiku čuvanje, mijenjanje, slanje i prosljeđivanje stanja aplikacije.

 \chapter{Implementacija sustava}
 
 Zbog veličine sustava i primijenjenih industrijskih standarda nije potrebno prolaziti kroz čitavu implementaciju.
 U ovom poglavlju bit će objašnjeni neki zahtjevniji implementacijski detalji platforme te oblikovni obrasci koji se pojavljuju.
 
 \section{Oblikovni obrasci}
 
Oblikovni obrasci su standardna rješenja za razred čestih problema u pojedinim problemskih domenama. \\
Na platformi su za različite sustave korištene 2 programske paradigme: objektno-orijentirano imperativno programiranje te funkcijsko deklarativno programiranje.

Objektno-orijentirano programiranje jest pristup programiranju kojeg karakterizira polimorfizam, enkapsulacija, apstrakcija, objekti te nasljeđivanje.

Imperativna paradigma jest pristup programiranju u kojemu nam je fokus na davanju uputa računalu kako obaviti neke zadatke, odnosno implementaciji algoritma. Definiramo slijedne korake te međuvrijednosti pohranjujemo u varijable

Funkcijsko programiranje jest pristup programiranju gdje promatramo program kao niz matematičkih funkcija te izbjegavamo stanje i mutacije.

Deklarativno programiranje je paradigma programiranja koji opisuje logiku računice bez opisivanje njenog kontrolnog toka. Dobar primjer te paradigme jest SQL.


\subsection{Strategija}

Oblikovni obrazac strategija se vrlo često koristi u objektno-orijentiranom programiranju.
Pripada skupini ponašajnih obrazaca te omogućuje odabir algoritma tijekom izvođenja programa.

Obrazac je korišten prilikom odabira upravljačkog programa \engl{driver} za bazu podataka te prilikom odabira hashing algoritma za lozinke u bazi podataka.

\subsection{Most}

Oblikovni obrazac most \engl{Bridge} korišten je u klijentskoj aplikaciji Test situacijske svjesnosti kako bi odvojio implementaciju prikazne logike \engl{render} od logike izvođenja testa.\\
Pripada skupini strukturnih obrazaca.

\begin{figure}[H]
\centering
\includegraphics[width=14cm]{img/point_renderer_class_uml}
\caption{UML dijagram klase PointRenderer}
\label{fig:point_renderer_uml}
\end{figure}

Objekti instance klase PointRenderer se koriste tako da pozivamo metodu accept s parametrima koji označavaju poziciju točke, njeno stanje te blok koda \engl{closure} koji se poziva klikom na nju.\\
Pozivom metode accept objekt PointRenderer iscrtava na \emph{canvas} element danu točku.
 
 \subsection{Injekcija ovisnosti}
 
Oblikovni obrazac injekcija ovisnosti \engl{dependency injection} jest široko korištena tehnika kojom se ostvaruje inverzija kontrole \engl{inversion of control}.
On znatno olakšava testiranje komponenti, potiče dobar dizajn jer pomaže smanjiti dubinu stabla ovisnosti te pospješuje primjećivanje cikličkih ovisnosti.
Koristi se u velikom dijelu klasa serverske aplikacije te klijentske aplikacije Test situacijske svjesnosti.

\begin{lstlisting}
class Game {

  private readonly collisions: Collision[];
  private readonly movements: Movement[];
  private readonly configuration: Configuration;
  private readonly valueGenerator: ValueGenerator;
  private currentTime: number;

  constructor(configuration: Configuration,
              valueGenerator: ValueGenerator,
              currentTime: number) {
    this.collisions = [];
    this.movements = [];
    this.configuration = configuration;
    this.valueGenerator = valueGenerator;
    this.currentTime = currentTime ?? 0;
    this.init();
  }
  ...
}
\end{lstlisting}

U gornjem isječku koda se vidi primjer upotrebe injekcije ovisnosti.
Modul Game ovisi o modulima ValueGenerator te Configuration, no ne instancira ih sam nego prima instance od roditeljske komponente.
Upotreba tog obrasca je znatno olakšala pisanje testova za komponentu Game.

\subsection{Promatrač}

Oblikovni obrazac promatrač \engl{observer} je jedan od najčešće korištenih obrazaca u JavaScriptu. \\

\begin{lstlisting}
class CanvasManager {
...
  public switch(clickEventHandler: (position: Position) => void) {
    // @ts-ignore
    this.drawingCanvas.getContext("2d").drawImage(this.bufferCanvas);
    this.drawingCanvas.onclick = ev => clickEventHandler(ev.x, ev.y);
    this.bufferCanvas = this.generateBuffer();
  }
 ...
\end{lstlisting}

U isječku koda dodajemo na \emph{canvas} element promatrača klikova pokazivačem.

\section{Implementacija zahtjeva platforme}

\subsection{Generator pseudoslučajnih brojeva}

Generator pseudoslučajnih vrijednosti je implementiran na sljedeći način:
\lstset{language=Javascript, tabsize=2}
\begin{lstlisting}
class ValueGenerator {

  private movementEdgePointSeed: string;
  private movementVisibleTimeSeed: string;
  private collisionTimeSeed: string;
  private collisionPositionSeed: string;

  constructor(seed: string) {
    this.collisionPositionSeed = sha1(seed + "1");
    this.collisionTimeSeed = sha1(seed + "2");
    this.movementVisibleTimeSeed = sha1(seed + "3");
    this.movementEdgePointSeed = sha1(seed + "4");
  }

  public getNextMovementEdgePoint(
      isFine: (position: Position) => boolean, 
      maxX: number, 
      maxY: number): Position {
    const getPosition = () => {
      const number = this.extractNumber(movementEdgePointSeed);
      const side = number % 4;
      switch (side) {
        case 0:
          return new Position(0, number % maxY)
        case 1:
          return new Position(number % maxX, maxY)
        case 2:
          return new Position(maxX, number % maxY)
        default:
          return new Position(number % maxX, 0);
      }
    }
    let position = getPosition();
    this.movementEdgePointSeed = sha1(this.movementEdgePointSeed);
    while (!isFine(position)) {
      position = getPosition();
      this.movementEdgePointSeed = sha1(this.movementEdgePointSeed);
    }
    return position;
  }

...

  private static extractNumber(source: string): number {
    return parseInt(source.substring(0, 8), 16)
  }
}
\end{lstlisting}

Kao što je vidljivo iz koda, prvo se koristeći inicijalnu seed vrijednost generiraju 4 nove seed vrijednosti.
Svaka od te 4 seed vrijednosti služi jednoj od 4 generativne funkcije.
Gore prikazana metoda \emph{getNextMovementEdgePoint(...)} se koristi za generiranje točaka \emph{T(x, y) takvih da:  ($x \in \{ 0, X_{max} \} \vee y \in \{ 0, Y_{max}\}) \wedge isFine(T(x,x)) $}
Parametar isFine je blok koda koji vraća logičko \textbf{DA} ili \textbf{NE} zavisno od toga ispunjava li generirana koordinata željene uvjete.

U ovom slučaju je primijenjena tehnika \textbf{grube sile} \engl{brute-force}, to jest \textbf{iscrpne pretrage} za dobivanje željene vrijednosti.
Razlog tome je što je problem teško riješiti analitički, a aplikacija ne gubi previše na performansi.
Koristeći profilizator koda je utvrđeno da su gubici na performansi prihvatljivi (proces se zadržava u dijelu koda ispod 1ms), odnosno događaju se samo pri pokretanju programa što je prihvatljivo u ovom slučaju.

\subsection{Prostorna hash mapa}

Sljedeći isječak koda pri svakom generiranju prikaza puni prostornu hash mapu, to jest rijetki array, ovdje nazvanu hitMapX:
\begin{lstlisting}
const xStart = Math.floor(position.x - pointRadius * 2);
const yStart = Math.floor(position.y - pointRadius * 2);
for (let i = 0; i < this.configuration.pointRadius * 4 + 1; i++) {
  for (let j = 0; j < pointRadius * 4 + 1; j++) {
    if (this.hitMapX[xStart + i]) {
      this.hitMapX[xStart + i][yStart + j] = onClick
    } else {
      this.hitMapX[xStart + i] = {[yStart + j]: onClick}
    }
  }
}
\end{lstlisting}

Dobivenu stukturu podataka se dalje u programu koristi na sljedeći način:
\begin{lstlisting}
...
  const clickHandler = ({x, y}) => this.hitMapX[x]?.[y]?.();
...
  canvas.onclick = clickHandler;
...
\end{lstlisting}

\subsection{Dvostruki spremnik}

Aplikacija koristi \emph{Canvas API} unutar preglednika za prikaz stanja stoga je koristeći njega implementiran dvostruki spremnik.\\
Sljedeći isječak koda jest komponenta zadužena za upravljanje spremnicima prikaza:
\begin{lstlisting}
class CanvasManager {

  private readonly configuration: Configuration;
  private bufferCanvas: HTMLCanvasElement;
  private drawingCanvas: HTMLCanvasElement;

  constructor(configuration: Configuration,
              drawingCanvas: HTMLCanvasElement) {
    this.configuration = configuration;
    this.drawingCanvas = drawingCanvas;
    this.bufferCanvas = this.generateBuffer();
  }

  public getBuffer(): CanvasRenderingContext2D {
    return  this.bufferCanvas.getContext("2d");
  }

  public switch(clickEventHandler: (position: Position) => void) {
    // @ts-ignore
    this.drawingCanvas.getContext("2d").drawImage(this.bufferCanvas);
    this.drawingCanvas.onclick = ev => clickEventHandler(ev.x, ev.y));
    this.bufferCanvas = this.generateBuffer();
  }


  private generateBuffer(): HTMLCanvasElement {
    const canvas = document.createElement("canvas")
    canvas.width = this.configuration.gameWidth;
    canvas.height = this.configuration.gameHeight;
    return canvas;
  }
}
\end{lstlisting}

Metoda getBuffer() vraća pomoćni spremnik u koji se crta, dok metoda switch(clickHandler) postavlja trenutni pomoćni spremnik kao glavni, 
\emph{canvas} elementu postavlja novi blok koda za procesuiranje klikova pokazivačem te generira novi pomoćni spremnik.

\subsection{Priručna spremišta}

U implementaciji platforme \emph{caching} se koristi na serverskom dijelu aplikacije, prilikom dohvata vrijednosti iz baze.

\lstset{language=Java, tabsize=2}
\begin{lstlisting}
@Cacheable("CollisionTestInstances")
public CollisionDetectionTemplate getTest(Long gameID) {
	return collisionDetectionScoreRepository.findById(gameID)
		.orElseThrow(() -> new NoSuchGameFoundException(
		    "No game with ID: " + gameID + " found"))
		.getTemplate();
}
\end{lstlisting}

U prethodnom primjeru \emph{caching} je ostvaren koristeći anotaciju \emph{@Cacheable}.
Bilo je dovoljno definirati naziv priručne memorije, a ostatak funkcionalnosti je već implementiran u radnom okviru \engl{framework} Spring Boot.

\section{Korištenje platforme}

\subsection{Upravljačko sučelje}

Upravljačko sučelje je klijentska aplikacija namijenjena istraživaču za spremanje obrazaca za generiranje testova,
generiranje instanci testova, njihovo brisanje, uređivanje i slično.

\begin{figure}[H]
\centering
\includegraphics[width=14cm]{img/starting_screen_dashboard}
\caption{Sučelje za generiranje novog obrasca ili testa}
\label{fig:admin_generate}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[width=14cm]{img/new_template_dashboard}
\caption{Generiranje novog testnog obrasca}
\label{fig:admin_template_generate}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[width=14cm]{img/generated_test_dashboard}
\caption{Rezultat generiranja novog testa, link se šalje ispitaniku}
\end{figure}

\subsection{Test situacijske svjesnosti}

Test situacijske svjesnosti jest klijentska aplikacija namijenjena ispitaniku za provedbu testa.

\begin{figure}[H]
\centering
\includegraphics[width=14cm]{img/test_start}
\caption{Početni ekran pri pokretanju testa}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[width=14cm]{img/test_inprogress}
\caption{Sučelje testa}
\end{figure}

Klikom na gumb start aplikacija preuzima čitav ekran \engl{fullscreen mode} te plave točkice postaju osjetljive na klik pokazivačem.
Istekom vremena definiranog prilikom generiranja testa aplikacija automatski izlazi iz načina rada u punom ekranu, šalje podatke serverskoj aplikaciji te daje povratnu informaciju ispitaniku u kojoj se zahvaljuje na sudjelovanju ili javlja da je došlo do greške pri spremanju rezultata te nudi čitljiv ispis rezultata kojega potom istraživač može ručno unijeti u bazu.

\chapter{Zaključak}
Izrada programske podrške za provedbu testiranja kognitivnih sposobnosti lako postane kompliciran zadatak.
Za razliku od običnih igara kognitivni testovi moraju biti ponovljivi i izuzetno konfigurabilni.

Dobro osmišljena arhitektura, primjena industrijskih standarda i dobra dokumentacija, bilo u obliku dokumenata s opisom sustava ili automatskih testova su nužni preduvjeti za proširivu, održivu platformu i njen brzi razvoj.
Kroz rad na platformi se pokazalo da neovisno o okolini izvođenja programa (Java virtualni stroj na serveru, JavaScript engine u browseru), okviru unutar kojega se piše kod (Spring Boot na serverskoj aplikaciji, React na upravljačkom sučelju, isključivo Browser API u Testu situacijske svjesnosti) ili jeziku u kojem se piše (Java u serverskoj aplikaciji, JavaScript u upravljačkom sučelju, Typescript u implementaciji testa) primjena inženjerskih načela dobrog oblikovanja programske podrške pomaže pri stvaranju kvalitetnog, validnog i pouzdanog programskog proizvoda za manje vremena nego što bi bilo potrebno da se istih načela nije pridržavalo.

\bibliography{literatura.bib}
\bibliographystyle{fer}

\begin{sazetak}
Situacijska svjesnost je sposobnost percipiranja elemenata okoline u vremenu i prostoru te njihovog projiciranja u budućnost.
U radu su definirani funkcionalni i nefunkcionalni zahtjevi programske potpore za provedbu testa situacijske svjesnosti te implementacija platforme koja odgovara zahtjevima.
Pri izradi platforme primijenjena su načela programskog inženjerstva te su u pisanom radu elaborirana.

\kljucnerijeci{Situacijska svjesnost, React, Spring Boot, Java, JavaScript, Kognitivne performanse}
\end{sazetak}

\engtitle{Upgradeable network platform for measuring situational awareness}
\begin{abstract}
Situational awareness is ability to perceive environmental elements in spatial and temporal domains and their projection in future.
Functional and non-functional requirements of a software system designed to aid in testing situational awareness have been identified and implemented as result of work.
Principles of software engineering have been applied in process of designing and implementing platform.
Application of principles of software engineering has been elaborated in thesis.

\keywords{Situational awareness, React, Spring Boot, Java, JavaScript, Cognitive performance}
\end{abstract}

\end{document}
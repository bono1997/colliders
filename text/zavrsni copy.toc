\babel@toc {croatian}{}
\contentsline {chapter}{\numberline {1}Uvod}{1}%
\contentsline {chapter}{\numberline {2}Pojednostavljena simulacija zračnog prometa}{2}%
\contentsline {section}{\numberline {2.1}Ponovljivost}{3}%
\contentsline {section}{\numberline {2.2}Brzina izvođenja}{4}%
\contentsline {subsection}{\numberline {2.2.1}Prostorna hash mapa}{5}%
\contentsline {subsection}{\numberline {2.2.2}Dvostruki spremnik}{5}%
\contentsline {subsection}{\numberline {2.2.3}Priručna spremišta}{5}%
\contentsline {section}{\numberline {2.3}Konfigurabilnost}{5}%
\contentsline {section}{\numberline {2.4}Preciznost}{6}%
\contentsline {section}{\numberline {2.5}Višekorisnički rad}{7}%
\contentsline {chapter}{\numberline {3}Arhitektura sustava}{9}%
\contentsline {section}{\numberline {3.1}Baza podataka}{10}%
\contentsline {section}{\numberline {3.2}Serverska aplikacija}{11}%
\contentsline {subsection}{\numberline {3.2.1}Model podataka}{11}%
\contentsline {subsection}{\numberline {3.2.2}Servisni sloj}{14}%
\contentsline {subsection}{\numberline {3.2.3}Kontrolerski sloj}{14}%
\contentsline {section}{\numberline {3.3}Test situacijske svjesnosti}{15}%
\contentsline {section}{\numberline {3.4}Upravljačko sučelje}{17}%
\contentsline {chapter}{\numberline {4}Implementacija sustava}{19}%
\contentsline {section}{\numberline {4.1}Oblikovni obrasci}{19}%
\contentsline {subsection}{\numberline {4.1.1}Strategija}{19}%
\contentsline {subsection}{\numberline {4.1.2}Most}{20}%
\contentsline {subsection}{\numberline {4.1.3}Injekcija ovisnosti}{20}%
\contentsline {subsection}{\numberline {4.1.4}Promatrač}{21}%
\contentsline {section}{\numberline {4.2}Implementacija zahtjeva platforme}{22}%
\contentsline {subsection}{\numberline {4.2.1}Generator pseudoslučajnih brojeva}{22}%
\contentsline {subsection}{\numberline {4.2.2}Prostorna hash mapa}{23}%
\contentsline {subsection}{\numberline {4.2.3}Dvostruki spremnik}{24}%
\contentsline {subsection}{\numberline {4.2.4}Priručna spremišta}{25}%
\contentsline {section}{\numberline {4.3}Korištenje platforme}{26}%
\contentsline {subsection}{\numberline {4.3.1}Upravljačko sučelje}{26}%
\contentsline {subsection}{\numberline {4.3.2}Test situacijske svjesnosti}{27}%
\contentsline {chapter}{\numberline {5}Zaključak}{29}%
\contentsline {chapter}{Literatura}{30}%
